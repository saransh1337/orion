
  function getSelectedParagraphText() {
    if (window.getSelection) {
        selection = window.getSelection();
    } else if (document.selection) {
        selection = document.selection.createRange();
    }
    var parent = selection.anchorNode;
    while (parent != null && parent.localName != "P") {
      parent = parent.parentNode;
    }
    if (parent == null) {
      return "";
    } else {
      return parent.innerText || parent.textContent;
    }
  }

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}

document.addEventListener('DOMContentLoaded', function() {
  var text = "";
  renderStatus("text");
});


addEventListener("keydown", function(event) {
  var text = "";
  if (event.keyCode == 87 || event.keyCode == 119){
    text = getSelectedParagraphText();
  }
  console.log("text");

});
